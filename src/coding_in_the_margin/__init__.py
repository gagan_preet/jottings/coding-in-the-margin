"""Coding in the Margin.

This is a package that provides a packaged and package-optimized codebase
for `Jotting in the Margin`_.

This module will be used as a base for the CLI.

.. _Jotting in the Margin
   http://jotting.gagan-preet.com/
"""
import click


__version__ = "0.1.0"


@click.command()
@click.version_option(version=__version__)
def main():
    """The Coding in the Margin project."""
    click.echo("Hello, world!")
