Welcome to Coding In The Margin's documentation!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   license
   reference



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
