"""Testing the main CLI for the Coding in the Margin package."""
import click.testing
import pytest

from coding_in_the_margin import main


@pytest.fixture
def runner():
    return click.testing.CliRunner()


def test_main_succeeds(runner):
    result = runner.invoke(main)
    assert result.exit_code == 0


def test_main_succeeds(runner):
    result = runner.invoke(main)
    expected = "Hello, World!"
    assert result.output == expected
