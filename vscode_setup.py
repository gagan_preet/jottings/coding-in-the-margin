#!/usr/bin/env python3
"""Setup script for VSCode."""

import json
import os
from pathlib import Path
import subprocess  # noqa: S404
from typing import Any, Dict

venv_path = (
    subprocess.check_output("poetry env info --path".split())  # noqa: S603
    .decode("UTF-8")
    .strip()
)

settings: Dict[str, Any] = dict()

Path(".vscode").mkdir(parents=True, exist_ok=True)
Path(".vscode/settings.json").touch()


if os.stat(".vscode/settings.json").st_size != 0:
    with open(".vscode/settings.json", "r") as f:
        settings = json.load(f)
settings["python.pythonPath"] = venv_path

with open(".vscode/settings.json", "w") as f:
    json.dump(settings, f, sort_keys=True, indent=4)


print(json.dumps(settings, sort_keys=True, indent=4))
